CC=gcc
CFLAGS=-lpcap -lpthread
DEPS=pcap.h arp.h ethernet.h
OBJ=arp.o ethernet.o arpscan.o

%.o: %.c $(DEPS)
	$(CC) -c $@
arpscan: $(OBJ)
	$(CC) -o arpscan arp.o ethernet.o arpscan.o $(CFLAGS)

.PHONY: clean

clean:
	rm -f *.o
