#include<stdio.h>
#include<string.h>
#include<stdint.h>
#include<arpa/inet.h>
#include"headers/ethernet.h"
#include"headers/arp.h"

/* Packages an ARP header into the given buffer, buffer point after
 * the link-layer header */
void packageARP(uint8_t *buffer, uint16_t op, uint8_t *sha,
        uint8_t *spa, uint8_t *tha, uint8_t *tpa)
{
    memset(buffer, 0, sizeof(struct arp_hdr));
    struct arp_hdr *arp = (struct arp_hdr *) buffer;
    arp->htype = htons(0x0001);
    arp->ptype = htons(0x0800);
    arp->hlen = ETHER_ADDRESS_LEN;
    arp->plen = 4;
    arp->op = htons(op);
    memcpy(arp->sha, sha, arp->hlen);
    memcpy(arp->spa, spa, arp->plen);
    memcpy(arp->tha, tha, arp->hlen);
    memcpy(arp->tpa, tpa, arp->plen);
}

/* Displays ARP header information from supplied packet
 * packet should point to start of header */
void parse_arp(const unsigned char *header_start)
{
    struct arp_hdr *arp = (struct arp_hdr *) header_start;
    int i;

    printf("\n===== ARP Header =====\n");
    printf("Hardware type: %04X\n", arp->htype);
    printf("Protocol type: %04X\n", arp->htype);
    printf("Hardware address length: %d octets\n", arp->hlen);
    printf("Protocol address length: %d octets\n", arp->plen);
    
    unsigned short op = ntohs(arp->op);
    printf("Opcode: 0x%04X - ", op);
    if(op == 1)
        printf("REQUEST\n");
    else
        printf("REPLY\n");

    printf("Source protocol address: ");
    for(i = 0; i < arp->plen-1; i++)
        printf("%d.", arp->spa[i]);
    printf("%d\n", arp->spa[i]);
    
    printf("Source hardware address: ");
    for(i = 0; i < arp->hlen-1; i++)
        printf("%02X:", arp->sha[i]);
    printf("%02X\n", arp->sha[i]);

    printf("Target protocol address: ");
    for(i = 0; i < arp->plen-1; i++)
        printf("%d.", arp->tpa[i]);
    printf("%d\n", arp->tpa[i]);

    printf("Target hardware address: ");
    for(i = 0; i < arp->hlen-1; i++)
        printf("%02X:", arp->tha[i]);
    printf("%02X\n", arp->tha[i]);
}
