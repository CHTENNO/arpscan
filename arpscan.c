#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<pthread.h>
#include<pcap.h>
#include<netinet/if_ether.h>
#include<arpa/inet.h>
#include<netdb.h>
#include<unistd.h>
#include"headers/ethernet.h"
#include"headers/arp.h"

long g_REPLY_SLEEP = 10000;
long g_SEND_SLEEP = 1000;
char g_VERBOSE = 0;

void usage() 
{
    printf("\n---- ARPSCAN ----\n");
    printf("-d:\tNIC\n");
    printf("-v:\tShow verbose messages\n");
    printf("-c:\tOutput as CSV\n");
    printf("-k:\tSet separator character for output");
    printf("-s:\tSubnet\n");
    printf("-p:\tSource protocol address\n");
    printf("-w:\tSource hardware address\n");
    printf("-m:\tMask\n");
    printf("-l:\tDNS lookup\n");
    printf("-x:\tMAC vendor lookup, takes path to OUIs as argument\n");
    printf("-r:\tTime to sleep while waiting for replies in microseconds\n");
    printf("-a:\tTime to sleep between sending arp requests in microseconds\n");
    printf("-h:\tDisplay this message\n\n");
}

int main(int argc, char *argv[])
{   
    /* Start of timer */
    time_t start, end;
    time(&start);

    char *subnet=NULL, *mask=NULL, *device=NULL, *source=NULL, *hwaddr=NULL;
    uint32_t spa;

    pcap_if_t *interface;
    pcap_if_t *alldevs; 
    char errbuf[PCAP_ERRBUF_SIZE];

    if(pcap_findalldevs(&alldevs, errbuf)) {
        fprintf(stderr, "Error: pcap_findalldevs %s\n", errbuf);
        exit(1);
    }
    
    interface = alldevs;

    char csv = 0;
    char timed = 0;
    char lookup = 0;
    char maclookup = 0;
    char *ouipath = NULL;
    
    char sep = '\t';

    /* Parse command line arguments */
    while(--argc) {
        if(*(++argv)[0] == '-') {
            switch(*++argv[0]) {
                /* Show verbose messages */
                case 'v':
                    g_VERBOSE = 1;
                    break;
                /* Specify device */
                case 'd':
                    device = *(++argv);
                    --argc;
                    break;
                /* Specify subnet */
                case 's':
                    subnet = *(++argv);
                    --argc;
                    break;
                /* Specify mask */
                case 'm':
                    mask = *(++argv);
                    --argc;
                    break;
                /* Specify source protocol address */
                case 'p':
                    source = *(++argv);
                    --argc;
                    break;
                case 'w':
                    hwaddr = *(++argv);
                    --argc;
                    break;
                /* Output as CSV */
                case 'c':
                    csv = 1;
                    break;
                /* Output with sep*/
                case 'k':
                    sep = *(++argv)[0];
                    --argc;
                    break;
                /* DNS Lookup */
                case 'l':
		    lookup = 1;
                    break;
                /* MAC Lookup */
                case 'x':
		    maclookup = 1;
                    ouipath = *(++argv);
                    --argc;
                    break;
                case 'r':
                    g_REPLY_SLEEP = atol(*(++argv));
                    --argc;
                    break;
                case 'a':
                    g_SEND_SLEEP= atol(*(++argv));
                    --argc;
                    break;
                /* Print timer */
                case 't':
                    timed = 1;
                    break;
                /* Print usage */
                case 'h':
                    usage();
                    exit(1);
                default:
                    usage();
                    exit(1);
            }
        }
    }

    if(!subnet || !mask) {
        fprintf(stderr, "Error: subnet and subnet mask must be specified.\n");
        usage();
        exit(1);
    }

    if(device != NULL) {
        while(interface != NULL && (strcmp(interface->name, device)))
            interface = interface->next;

        if(!interface) {
            fprintf(stderr, "Error: Device %s not found.\n", device);
            exit(1);
        }
    }

    if(!source) {
        pcap_addr_t *addr;
        for(addr = interface->addresses; 
                addr != NULL && addr->addr->sa_family != AF_INET; addr = addr->next)
            ;
        if(addr != NULL)
            memcpy(&spa, &((struct sockaddr_in *)addr->addr)->sin_addr.s_addr, sizeof(uint32_t));
        else {
            fprintf(stderr, "Error: No IPv4 address associated with interface %s.\n", interface->name);
            exit(1);
        }
    }
    else {
        inet_pton(AF_INET, source, &spa);
    }

    uint8_t sha[6];

    if(!hwaddr) {
        if (!get_hardware_address(interface->name, sha)) {
            fprintf(stderr, "Error: Could not obtain hardware address of %s\n.", interface->name);           
            exit(1); 
        }
    }
    else {
        str_to_mac(hwaddr, sha);
    }



    /* To get the number of address in the subnet
     * invert the subnet mask */
    in_addr_t u_mask;
    inet_pton(AF_INET, mask, &u_mask);
    unsigned long noofaddr = ntohl(~u_mask) + 1;

    /* Arguments for the sniffing thread */
    /* Arguments must be in the order
     * [0] device
     * [1] subnet mask
     * [2] address table */
    char **sniff_args = (char **) malloc(3 * sizeof(char *));
    sniff_args[0] = (char *) malloc(20);
    sniff_args[1] = (char *) malloc(20);
    sniff_args[2] = (char *) malloc(20);

    /* Address table to store the hardware addresses for each
     * IP address,  if the subnet is 192.168.0.0 and the mask is
     * 255.255.255.0 then hwaddrs[65] is the hardware address for 
     * 192.168.0.65 if the mask. */
    char **table = (char **) malloc(sizeof(char *) * noofaddr);
    for(int i = 0; i < noofaddr; i++)
        table[i] = (char *) calloc(20, sizeof(char));

    strcpy(sniff_args[0], interface->name);
    strcpy(sniff_args[1], mask);
    sniff_args[2] = (char *) table;

    /* Arguments for the sending thread */
    /* Arguments must be in the order
     * [0] device
     * [1] subnet
     * [2] subnet mask
     * [3] source hardware address
     * [4] source protocol address */
    char **send_args = (char **) malloc(5 * sizeof(char *));
    send_args[0] = (char *) malloc(20);
    send_args[1] = (char *) malloc(20);
    send_args[2] = (char *) malloc(20);
    send_args[3] = (char *) malloc(20);
    send_args[4] = (char *) malloc(20);

    /* Device */
    strcpy(send_args[0], interface->name);
    /* Subnet */
    strcpy(send_args[1], subnet);
    /* Subnet mask */
    strcpy(send_args[2], mask);
    /* Source hardware address */
    memcpy(send_args[3], sha, ETHER_ADDRESS_LEN);
    /* Source protocol address */
    memcpy(send_args[4], (void *) &spa, sizeof(void *));


    /* PIDs for sending a sniffing the ARP packets */
    pthread_t send_pid;
    pthread_t sniff_pid;
 
    /* prototype for function used to send ARP requests */
    void *sendrequest(void *);
    /* prototype for function used to sniff for ARP replys */
    void *sniffreplys(void *);
    void getARPData(u_char *, const struct pcap_pkthdr *, const u_char *);

    /* Thread for sniffing the ARP replys */
    pthread_create(&sniff_pid, NULL, sniffreplys, (void **) sniff_args);
    if(g_VERBOSE)
        printf("Sniffing thread created.\n");
    /* Thread for sending the ARP requests */
    pthread_create(&send_pid, NULL, sendrequest, send_args);
    if(g_VERBOSE)
        printf("ARP thread created.\n");

    /* Join the sending thread to this thread */
    pthread_join(send_pid, NULL);
    
    if(g_VERBOSE)
        printf("All ARP requests sent.\n");

    /* Now the sending thread has stopped we will wait 10ms
     * for ARP replys and then end */
    usleep(g_REPLY_SLEEP);
    pthread_cancel(sniff_pid);
    pthread_join(sniff_pid, NULL);

    if(g_VERBOSE)
        printf("Scan finished listing table:\n");

    uint32_t u_sub;
    inet_pton(AF_INET, subnet, &u_sub);
    uint32_t first = u_sub & u_mask;
    
    if(g_VERBOSE)
        printf("Subnet mask created...");

    for(int i = 0; i < noofaddr; i++) {
        if(*(table[i]) != 0) {

            char str[20];
            sprintf(str, "%d.%d.%d.%d", 
                    first & 0xFF,
                    (first >> 8) & 0xFF,
                    (first >> 16) & 0xFF,
                    (first >> 24) & 0xFF);

            /* Perform reverse DNS lookup on IP Address */
            struct hostent *host = NULL;


	    if(lookup) {
	    	if(g_VERBOSE)
			printf("Reverse lookup: %s...\n", str);
		host = gethostbyaddr(&first, sizeof(first), AF_INET);
	    }

            if(csv)
                sep = ',';

            /* Output results */
            if(maclookup) {
	        if(g_VERBOSE)
                    printf("Perfoming mac lookup...");
                char result[512];
                getvendor(table[i], result, ouipath);
                printf("%s%c%s%c%s%c", str, sep, table[i], sep, result, sep);
            }
            else
                printf("%s%c%s%c", str, sep, table[i], sep);
            
            /* If DNS lookup was successful and h_name is not NULL print it */
            if(host != NULL)
               	printf("%s\n", host->h_name);
            else
               	printf("NO_HOSTNAME\n");
        }
        first = ntohl(first);
        ++first;
        first = htonl(first);
    }

    /* Stop timer */
    time(&end);

    if(timed)
        printf("\n%ld seconds.\n", end - start);

    /* Free memory allocated from malloc() */
    free(sniff_args);
    free(send_args);
    free(table);
    pthread_exit(0);
}

/*
 * Function for sending the arp requests. Sends 5 sets of arp
 * requests.
 */
void *sendrequest(void *args)
{
    char **strings = (char **) args;
    int r;
    /* buffer for constructing packet */
    uint8_t buffer[4096];
    /* The device supplied in the arguments */
    char *device = strings[0];
    /* the subnet supplied in arguements */
    uint32_t subnet;
    inet_pton(AF_INET, strings[1], &subnet);
    /* the subnet mask supplied in arguements */
    uint32_t mask;
    inet_pton(AF_INET, strings[2], &mask);
    /* machines own hardware address supplied in arguments */
    uint8_t *sha = (uint8_t *) strings[3];
    /* machines own ip address supplied in arguments */
    uint32_t *spa = (uint32_t *) strings[4];
    /* tha for the ARP packet is the broadcast address */
    uint8_t broadcast[ETHER_ADDRESS_LEN];
    str_to_mac("ff:ff:ff:ff:ff:ff", broadcast);

    /* pcap handle */
    pcap_t *handle;
    /* pcap error buffer */
    char errbuf[PCAP_ERRBUF_SIZE];
    /* open device */
    handle = pcap_open_live(device, 4096, 1, 100, errbuf);

    /* Last address = subnet | ~mask */
    uint32_t last = subnet | ~mask;

    for(int i=0; i<5; i++)
    {
        /* First address = subnet & mask */
        uint32_t tpa = subnet & mask;
        
        while(tpa <= last) {
            tpa = ntohl(tpa);
            tpa++;
            tpa = htonl(tpa);
            /* Package the ethernet header */
            packageEthernet(buffer, broadcast, sha, ETHER_PROTO_ARP);
            /* Package the arp header */
            packageARP(buffer+ETHER_HEADER_LEN, ARP_OPER_REQUEST,
                    sha, (uint8_t *) spa, broadcast, (uint8_t *) &tpa);
            /* Send the packet */
            if(pcap_sendpacket(handle, buffer, ETHER_HEADER_LEN+ARP_HEADER_LEN))
                fprintf(stderr, "pcap - error injecting packets %s\n", pcap_geterr(handle));
            /* Sleep 1000 microseconds between loops */
            usleep(g_SEND_SLEEP);
        }
    }

    pthread_exit(0);
}

/*
 * Callback function for pcap_loop(). Checks if the caught packet is an arp reply
 * and addres the MAC address to the table.
 */
void getARPData(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
    if(ntohs(arp_operation(packet+ETHER_HEADER_LEN)) == ARP_OPER_REPLY) {
        /* Get the subnet mask from the arguments */
        char *subnet = ((char **) args)[1];
        /* Get the table from the arguments */
        char **table = ((void **) args)[2];

        /* Get the protocol address and hardware address of the sender */
        u_char *spa = arp_spa(packet+ETHER_HEADER_LEN);
        u_char *sha = arp_sha(packet+ETHER_HEADER_LEN);

        if(g_VERBOSE) {
            printf("Reply from: %d.%d.%d.%d ----> ", spa[0], spa[1], spa[2], spa[3]);
            printf("%02X:%02X:%02X:%02X:%02X:%02X\n", sha[0], sha[1], sha[2], sha[3], sha[4], sha[5]);
        }

        /* Create the mask */
        in_addr_t mask;
        inet_pton(AF_INET, subnet, &mask);
        /* Copy the address into an integer */
        in_addr_t addr;
        memcpy(&addr, spa, 4);
        /* And the address with the mask to obtain the host and then 
         * convert to host byte order */
        uint32_t host = ntohl(~mask & addr);
        /* Format the hardware address as a string and add it to the table */
        sprintf(table[host], "%02X:%02X:%02X:%02X:%02X:%02X", sha[0], sha[1], sha[2], sha[3], sha[4], sha[5]);
    }
}

/*
 * Function for sniffing packets. Calls pcap_loop() which
 * uses getARPData().
 */
void *sniffreplys(void *args)
{    
    char errbuff[PCAP_ERRBUF_SIZE];

    /* Get device from arguments */
    char *device = ((char **) args)[0];

    /* Pcap descriptor */
    pcap_t *handle;
    
    /* Open device */
    if((handle = pcap_open_live(device, 4096, 1, 1000, errbuff)) == NULL) {
        fprintf(stderr, "Error in pcap_open_live()...\n");
        exit(1);
    }

    /* Loop after packers and call decode for each capture */
    pcap_loop(handle, -1, getARPData, args);

    pthread_exit(0);
}
