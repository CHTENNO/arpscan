#include<stdio.h>
#include<stdint.h>
#include<string.h>
#include<stdlib.h>
#include<arpa/inet.h>
#include<sys/ioctl.h>
#include<linux/if.h>
#include<unistd.h>
#include"headers/ethernet.h"

static char **OUIs = NULL;
static char **vendors = NULL;
static long entries = 0;

/* 
 * Displays ethernet header information from a given packet and returns
 * the ethertype in host byte order 
 */
uint16_t parse_ethernet(const uint8_t *header_start)
{
    struct ether_hdr *eth = (struct ether_hdr *) header_start;
    int i;

    printf("\n===== Ethernet header =====\n");

    /* Print out address byte by byte */
    printf("Destination address: ");
    for(i = 0; i < ETHER_ADDRESS_LEN-1; i++)
        printf("%02X:", eth->dest_addr[i]);
    printf("%02X\n", eth->dest_addr[i]);

    printf("Source Address: ");
    for(i = 0; i < ETHER_ADDRESS_LEN-1; i++)
        printf("%02X:", eth->src_addr[i]);
    printf("%02X\n", eth->src_addr[i]);

    uint16_t ethertype = ntohs(eth->proto);
    printf("Protcol: 0x%04X\n", ethertype);

    return ethertype;
}

/*
 * Packages an ethernet header into the given buffer
 */
void packageEthernet(uint8_t *buffer, uint8_t *dest,
        uint8_t *source, uint16_t proto)
{
    memset(buffer, 0, ETHER_HEADER_LEN);
    struct ether_hdr *eth = (struct ether_hdr *) buffer;                        
    memcpy(eth->dest_addr, dest, ETHER_ADDRESS_LEN);                                 
    memcpy(eth->src_addr, source, ETHER_ADDRESS_LEN);                                   
    eth->proto = htons(proto);
}

static char __hextoint(char c)
{
    if(c >= 'a' && c <= 'f')
        return c - 'a' + 10;
    else if (c >= 'A' && c <= 'F')
        return c - 'A' + 10;
    else if (c >= '0' && c <= '9')
        return c -'0';
    else
        return (char) 255;
}

/* 
 * Converts a string representation mac address to a pointer
 * to a uint8_t 
 * invokes malloc(), should free() pointer after use. 
 */
int str_to_mac(char *str, uint8_t *mac)
{
    if(strlen(str) != 17)
        return 0;

    uint8_t hex1, hex2;
    int i = 0;
    while(*str != '\0' && i < 6) {

        if((hex1 = __hextoint(*str++)) == 255 ||
                (hex2 = __hextoint(*str++)) == 255)
            return 0;

        mac[i++] = (hex1 << 4) + hex2;
        
        if(*str == ':')
            str++;
    }

    return 1;
}

/*
 * Returns the interface address of the given device name. Returns 1 on success
 * and 0 when not found / error.
 */
int get_hardware_address(char *interface, uint8_t *mac)
{
    int sfd;
    struct ifreq ifr;
    
    sfd = socket(AF_INET, SOCK_DGRAM, 0);
    strcpy(ifr.ifr_name, interface);

    if(ioctl(sfd, SIOCGIFHWADDR, &ifr) == -1) {
        close(sfd);
        return 0;
    }

    close(sfd);
    memcpy(mac, ifr.ifr_hwaddr.sa_data, ETHER_ADDRESS_LEN);

    return 1;
}

int whitespace(char c)
{
    return (c == ' ' || c == '\t' ? 1 : 0);
}

/*
 * Populates the arrays OUIs[] and vendors[] with the
 * data found in ouis.txt
 */
static void fillarrays(char *path)
{
    FILE *vendorList;
    if((vendorList = fopen(path, "r")) == NULL) {
        fprintf(stderr, "Error openning vendor list file\n");
        return;
    }

    for(char c = 'a'; !feof(vendorList); c = fgetc(vendorList)) {
        if(c == '\n')
            entries++;
    }

    OUIs = (char **) malloc(sizeof(char *) * entries);
    vendors = (char **) malloc(sizeof(char *) * entries);

    for(int i = 0; i < entries; i++) {
        OUIs[i] = (char *) malloc(sizeof(char) * 1024);
        vendors[i] = (char *) malloc(sizeof(char) * 1024);
    }

    rewind(vendorList);

    for(int i = 0; i < entries; i++) {
        char c;
        int j = 0;

        /* Store OUI in table */
        while(!whitespace((c = fgetc(vendorList))))
            OUIs[i][j++] = c;
        OUIs[i][j] = '\0';

        j = 0;

        /* Skip white space */
        while(whitespace((c = fgetc(vendorList))))
            ;

        fseek(vendorList, -1L, SEEK_CUR);

        /* Store vendor name in table */
        while((c = fgetc(vendorList)) != '\n' && c != '\r' && c != EOF)
            vendors[i][j++] = c;
        vendors[i][j] = '\0';
        
        /* If encountered carrige return skip until newline or EOF is read */
        if(c == '\r')
            while((c = fgetc(vendorList)) != '\n' && c != EOF)
                ;
    }

}

/*
 * Get the vendor from the OUI of a MAC address. Requires the
 * mac-vendor.txt file to be in the same directory
 */
void getvendor(char* mac, char* result, char *path)
{
    if(OUIs == NULL && vendors == NULL)
        fillarrays(path);

    char oui[7] = {mac[0], mac[1], mac[3], mac[4], mac[6], mac[7], '\0'};

    unsigned long upper, lower;
    lower = 0;
    upper = entries-1;

    strcpy(result, "Unknown vendor");

    while(lower <= upper) {
        unsigned long mid = (upper + lower) / 2;
        int r = strcmp(oui, OUIs[mid]);

        if(r < 0)
            upper = mid - 1;
        else if(r > 0)
            lower = mid + 1;
        else {
                strcpy(result, vendors[mid]);
                break;
        }
    }
}

