#ifndef ETHER_H
#define ETHER_H

#define ETHER_ADDRESS_LEN 6 /* Octets in the ethernet address */
#define ETHER_HEADER_LEN sizeof(struct ether_hdr)

#define ETHER_PROTO_IPV4        0x0800
#define ETHER_PROTO_ARP         0x0806
#define ETHER_PROTO_IPV6        0x86DD

/* Gives the ethertype of given ethernet header in network byte order */
#define ether_type_nbo(ethhdr) (((struct ether_hdr *)(ethhdr))->proto)

#include<stdint.h>

struct ether_hdr {
    uint8_t dest_addr[ETHER_ADDRESS_LEN];
    uint8_t src_addr[ETHER_ADDRESS_LEN];
    uint16_t proto;
} __attribute__((packed));

uint16_t parse_ethernet(const uint8_t *header_start);
void packageEthernet(uint8_t *buffer, uint8_t *dest, uint8_t *source, uint16_t proto);
int str_to_mac(char *str, uint8_t *);
int get_hardware_address(char *interface, uint8_t *mac);
void getvendor(char* mac, char* result, char *path);

#endif
